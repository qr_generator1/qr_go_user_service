package postgres

import (
	"app/genproto/user_service"
	"app/models"
	"app/pkg/helper"
	"app/storage"

	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserRepoI {
	return &UserRepo{
		db: db,
	}
}

func (c *UserRepo) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "users" (
				id,
				first_name,
				last_name,
				phone_number,
				email,
				country,
				website,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Email,
		req.Country,
		req.Website,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserPrimaryKey{UserId: id.String()}, nil
}

func (c *UserRepo) GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (*user_service.User, error) {

	query := `
		SELECT
			id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			phone_number,
			birthday,
			email,
			country,
			website,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "users"
		WHERE id = $1
	`
	var resp user_service.User

	err := c.db.QueryRow(ctx, query, req.GetUserId()).Scan(
		&resp.UserId,
		&resp.FirstName,
		&resp.LastName,
		&resp.PhoneNumber,
		&resp.Email,
		&resp.Country,
		&resp.Website,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *UserRepo) GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	resp = &user_service.GetListUserResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			birthday,
			email,
			country,
			website,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "users"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var user user_service.User

		err := rows.Scan(
			&resp.Count,
			&user.UserId,
			&user.FirstName,
			&user.LastName,
			&user.PhoneNumber,
			&user.Email,
			&user.Country,
			&user.Website,
			&user.CreatedAt,
			&user.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Users = append(resp.Users, &user)
	}

	return
}

func (c *UserRepo) Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)
	query = `
			UPDATE
			    "users"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				birthday = :birthday,
				email = :email,
				country  =:country,
				website =:website,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetUserId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"phone_number": req.GetPhoneNumber(),
		"email":        req.GetEmail(),
		"country":      req.GetCountry(),
		"website":      req.GetWebsite(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *UserRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"users"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *UserRepo) Delete(ctx context.Context, req *user_service.UserPrimaryKey) error {

	query := `DELETE FROM "users" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.UserId)

	if err != nil {
		return err
	}

	return nil
}

func (c *UserRepo) Login(ctx context.Context, req *user_service.LoginReq) (*user_service.User, error) {

	query := `
		SELECT
			id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			phone_number,
			birthday,
			email,
			country,
			website,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "users"
		WHERE first_name = $1 AND phone_number = $2
	`

	resp := user_service.User{}

	err := c.db.QueryRow(ctx, query, req.GetUsername(), req.GetPassword()).Scan(
		&resp.UserId,
		&resp.FirstName,
		&resp.LastName,
		&resp.PhoneNumber,
		&resp.Email,
		&resp.Country,
		&resp.Website,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	)

	fmt.Println(resp)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *UserRepo) ChechExist(ctx context.Context, req *user_service.CheckExist) (*user_service.CheckExistRes, error) {

	query := `SELECT COUNT(*) FROM users WHERE phone_number = $1`

	var count int

	err := c.db.QueryRow(ctx, query, req.GetPhoneNum()).Scan(
		&count,
	)

	if err != nil {
		return nil, err
	}

	if count >= 1 {
		return &user_service.CheckExistRes{Type: true}, nil
	} else {
		return &user_service.CheckExistRes{Type: false}, nil
	}
}
