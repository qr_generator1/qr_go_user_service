// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.6.1
// source: url_service.proto

package qr_type_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	UrlService_Create_FullMethodName      = "/qr_type_service.UrlService/Create"
	UrlService_GetById_FullMethodName     = "/qr_type_service.UrlService/GetById"
	UrlService_GetList_FullMethodName     = "/qr_type_service.UrlService/GetList"
	UrlService_Update_FullMethodName      = "/qr_type_service.UrlService/Update"
	UrlService_UpdatePatch_FullMethodName = "/qr_type_service.UrlService/UpdatePatch"
	UrlService_Delete_FullMethodName      = "/qr_type_service.UrlService/Delete"
)

// UrlServiceClient is the client API for UrlService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type UrlServiceClient interface {
	Create(ctx context.Context, in *CreateUrl, opts ...grpc.CallOption) (*Url, error)
	GetById(ctx context.Context, in *UrlPrimaryKey, opts ...grpc.CallOption) (*Url, error)
	GetList(ctx context.Context, in *GetListUrlRequest, opts ...grpc.CallOption) (*GetListUrlResponse, error)
	Update(ctx context.Context, in *UpdateUrl, opts ...grpc.CallOption) (*Url, error)
	UpdatePatch(ctx context.Context, in *UpdatePatchUrl, opts ...grpc.CallOption) (*Url, error)
	Delete(ctx context.Context, in *UrlPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error)
}

type urlServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewUrlServiceClient(cc grpc.ClientConnInterface) UrlServiceClient {
	return &urlServiceClient{cc}
}

func (c *urlServiceClient) Create(ctx context.Context, in *CreateUrl, opts ...grpc.CallOption) (*Url, error) {
	out := new(Url)
	err := c.cc.Invoke(ctx, UrlService_Create_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *urlServiceClient) GetById(ctx context.Context, in *UrlPrimaryKey, opts ...grpc.CallOption) (*Url, error) {
	out := new(Url)
	err := c.cc.Invoke(ctx, UrlService_GetById_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *urlServiceClient) GetList(ctx context.Context, in *GetListUrlRequest, opts ...grpc.CallOption) (*GetListUrlResponse, error) {
	out := new(GetListUrlResponse)
	err := c.cc.Invoke(ctx, UrlService_GetList_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *urlServiceClient) Update(ctx context.Context, in *UpdateUrl, opts ...grpc.CallOption) (*Url, error) {
	out := new(Url)
	err := c.cc.Invoke(ctx, UrlService_Update_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *urlServiceClient) UpdatePatch(ctx context.Context, in *UpdatePatchUrl, opts ...grpc.CallOption) (*Url, error) {
	out := new(Url)
	err := c.cc.Invoke(ctx, UrlService_UpdatePatch_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *urlServiceClient) Delete(ctx context.Context, in *UrlPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, UrlService_Delete_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UrlServiceServer is the server API for UrlService service.
// All implementations must embed UnimplementedUrlServiceServer
// for forward compatibility
type UrlServiceServer interface {
	Create(context.Context, *CreateUrl) (*Url, error)
	GetById(context.Context, *UrlPrimaryKey) (*Url, error)
	GetList(context.Context, *GetListUrlRequest) (*GetListUrlResponse, error)
	Update(context.Context, *UpdateUrl) (*Url, error)
	UpdatePatch(context.Context, *UpdatePatchUrl) (*Url, error)
	Delete(context.Context, *UrlPrimaryKey) (*empty.Empty, error)
	mustEmbedUnimplementedUrlServiceServer()
}

// UnimplementedUrlServiceServer must be embedded to have forward compatible implementations.
type UnimplementedUrlServiceServer struct {
}

func (UnimplementedUrlServiceServer) Create(context.Context, *CreateUrl) (*Url, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedUrlServiceServer) GetById(context.Context, *UrlPrimaryKey) (*Url, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedUrlServiceServer) GetList(context.Context, *GetListUrlRequest) (*GetListUrlResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedUrlServiceServer) Update(context.Context, *UpdateUrl) (*Url, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedUrlServiceServer) UpdatePatch(context.Context, *UpdatePatchUrl) (*Url, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePatch not implemented")
}
func (UnimplementedUrlServiceServer) Delete(context.Context, *UrlPrimaryKey) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedUrlServiceServer) mustEmbedUnimplementedUrlServiceServer() {}

// UnsafeUrlServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to UrlServiceServer will
// result in compilation errors.
type UnsafeUrlServiceServer interface {
	mustEmbedUnimplementedUrlServiceServer()
}

func RegisterUrlServiceServer(s grpc.ServiceRegistrar, srv UrlServiceServer) {
	s.RegisterService(&UrlService_ServiceDesc, srv)
}

func _UrlService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateUrl)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UrlServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: UrlService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UrlServiceServer).Create(ctx, req.(*CreateUrl))
	}
	return interceptor(ctx, in, info, handler)
}

func _UrlService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UrlPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UrlServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: UrlService_GetById_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UrlServiceServer).GetById(ctx, req.(*UrlPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _UrlService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListUrlRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UrlServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: UrlService_GetList_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UrlServiceServer).GetList(ctx, req.(*GetListUrlRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UrlService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateUrl)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UrlServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: UrlService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UrlServiceServer).Update(ctx, req.(*UpdateUrl))
	}
	return interceptor(ctx, in, info, handler)
}

func _UrlService_UpdatePatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePatchUrl)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UrlServiceServer).UpdatePatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: UrlService_UpdatePatch_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UrlServiceServer).UpdatePatch(ctx, req.(*UpdatePatchUrl))
	}
	return interceptor(ctx, in, info, handler)
}

func _UrlService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UrlPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UrlServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: UrlService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UrlServiceServer).Delete(ctx, req.(*UrlPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// UrlService_ServiceDesc is the grpc.ServiceDesc for UrlService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var UrlService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "qr_type_service.UrlService",
	HandlerType: (*UrlServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _UrlService_Create_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _UrlService_GetById_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _UrlService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _UrlService_Update_Handler,
		},
		{
			MethodName: "UpdatePatch",
			Handler:    _UrlService_UpdatePatch_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _UrlService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "url_service.proto",
}
