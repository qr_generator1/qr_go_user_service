// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.6.1
// source: wifi_service.proto

package qr_type_service

import (
	empty "github.com/golang/protobuf/ptypes/empty"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_wifi_service_proto protoreflect.FileDescriptor

var file_wifi_service_proto_rawDesc = []byte{
	0x0a, 0x12, 0x77, 0x69, 0x66, 0x69, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0f, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x1a, 0x0a, 0x77, 0x69, 0x66, 0x69, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0xb9,
	0x03, 0x0a, 0x0b, 0x57, 0x69, 0x66, 0x69, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3e,
	0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x1b, 0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79,
	0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x57, 0x69, 0x66, 0x69, 0x1a, 0x15, 0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x57, 0x69, 0x66, 0x69, 0x22, 0x00, 0x12, 0x43,
	0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x12, 0x1f, 0x2e, 0x71, 0x72, 0x5f, 0x74,
	0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x57, 0x69, 0x66, 0x69,
	0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x15, 0x2e, 0x71, 0x72, 0x5f,
	0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x57, 0x69, 0x66,
	0x69, 0x22, 0x00, 0x12, 0x56, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x23,
	0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x57, 0x69, 0x66, 0x69, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x57, 0x69, 0x66,
	0x69, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x3e, 0x0a, 0x06, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x1b, 0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x57, 0x69,
	0x66, 0x69, 0x1a, 0x15, 0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x57, 0x69, 0x66, 0x69, 0x22, 0x00, 0x12, 0x48, 0x0a, 0x0b, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x74, 0x63, 0x68, 0x12, 0x20, 0x2e, 0x71, 0x72, 0x5f,
	0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x50, 0x61, 0x74, 0x63, 0x68, 0x57, 0x69, 0x66, 0x69, 0x1a, 0x15, 0x2e, 0x71,
	0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x57,
	0x69, 0x66, 0x69, 0x22, 0x00, 0x12, 0x43, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12,
	0x1f, 0x2e, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x57, 0x69, 0x66, 0x69, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79,
	0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x1a, 0x5a, 0x18, 0x67, 0x65,
	0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x71, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_wifi_service_proto_goTypes = []interface{}{
	(*CreateWifi)(nil),          // 0: qr_type_service.CreateWifi
	(*WifiPrimaryKey)(nil),      // 1: qr_type_service.WifiPrimaryKey
	(*GetListWifiRequest)(nil),  // 2: qr_type_service.GetListWifiRequest
	(*UpdateWifi)(nil),          // 3: qr_type_service.UpdateWifi
	(*UpdatePatchWifi)(nil),     // 4: qr_type_service.UpdatePatchWifi
	(*Wifi)(nil),                // 5: qr_type_service.Wifi
	(*GetListWifiResponse)(nil), // 6: qr_type_service.GetListWifiResponse
	(*empty.Empty)(nil),         // 7: google.protobuf.Empty
}
var file_wifi_service_proto_depIdxs = []int32{
	0, // 0: qr_type_service.WifiService.Create:input_type -> qr_type_service.CreateWifi
	1, // 1: qr_type_service.WifiService.GetById:input_type -> qr_type_service.WifiPrimaryKey
	2, // 2: qr_type_service.WifiService.GetList:input_type -> qr_type_service.GetListWifiRequest
	3, // 3: qr_type_service.WifiService.Update:input_type -> qr_type_service.UpdateWifi
	4, // 4: qr_type_service.WifiService.UpdatePatch:input_type -> qr_type_service.UpdatePatchWifi
	1, // 5: qr_type_service.WifiService.Delete:input_type -> qr_type_service.WifiPrimaryKey
	5, // 6: qr_type_service.WifiService.Create:output_type -> qr_type_service.Wifi
	5, // 7: qr_type_service.WifiService.GetById:output_type -> qr_type_service.Wifi
	6, // 8: qr_type_service.WifiService.GetList:output_type -> qr_type_service.GetListWifiResponse
	5, // 9: qr_type_service.WifiService.Update:output_type -> qr_type_service.Wifi
	5, // 10: qr_type_service.WifiService.UpdatePatch:output_type -> qr_type_service.Wifi
	7, // 11: qr_type_service.WifiService.Delete:output_type -> google.protobuf.Empty
	6, // [6:12] is the sub-list for method output_type
	0, // [0:6] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_wifi_service_proto_init() }
func file_wifi_service_proto_init() {
	if File_wifi_service_proto != nil {
		return
	}
	file_wifi_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_wifi_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_wifi_service_proto_goTypes,
		DependencyIndexes: file_wifi_service_proto_depIdxs,
	}.Build()
	File_wifi_service_proto = out.File
	file_wifi_service_proto_rawDesc = nil
	file_wifi_service_proto_goTypes = nil
	file_wifi_service_proto_depIdxs = nil
}
