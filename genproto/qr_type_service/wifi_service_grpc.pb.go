// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.6.1
// source: wifi_service.proto

package qr_type_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	WifiService_Create_FullMethodName      = "/qr_type_service.WifiService/Create"
	WifiService_GetById_FullMethodName     = "/qr_type_service.WifiService/GetById"
	WifiService_GetList_FullMethodName     = "/qr_type_service.WifiService/GetList"
	WifiService_Update_FullMethodName      = "/qr_type_service.WifiService/Update"
	WifiService_UpdatePatch_FullMethodName = "/qr_type_service.WifiService/UpdatePatch"
	WifiService_Delete_FullMethodName      = "/qr_type_service.WifiService/Delete"
)

// WifiServiceClient is the client API for WifiService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type WifiServiceClient interface {
	Create(ctx context.Context, in *CreateWifi, opts ...grpc.CallOption) (*Wifi, error)
	GetById(ctx context.Context, in *WifiPrimaryKey, opts ...grpc.CallOption) (*Wifi, error)
	GetList(ctx context.Context, in *GetListWifiRequest, opts ...grpc.CallOption) (*GetListWifiResponse, error)
	Update(ctx context.Context, in *UpdateWifi, opts ...grpc.CallOption) (*Wifi, error)
	UpdatePatch(ctx context.Context, in *UpdatePatchWifi, opts ...grpc.CallOption) (*Wifi, error)
	Delete(ctx context.Context, in *WifiPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error)
}

type wifiServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewWifiServiceClient(cc grpc.ClientConnInterface) WifiServiceClient {
	return &wifiServiceClient{cc}
}

func (c *wifiServiceClient) Create(ctx context.Context, in *CreateWifi, opts ...grpc.CallOption) (*Wifi, error) {
	out := new(Wifi)
	err := c.cc.Invoke(ctx, WifiService_Create_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wifiServiceClient) GetById(ctx context.Context, in *WifiPrimaryKey, opts ...grpc.CallOption) (*Wifi, error) {
	out := new(Wifi)
	err := c.cc.Invoke(ctx, WifiService_GetById_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wifiServiceClient) GetList(ctx context.Context, in *GetListWifiRequest, opts ...grpc.CallOption) (*GetListWifiResponse, error) {
	out := new(GetListWifiResponse)
	err := c.cc.Invoke(ctx, WifiService_GetList_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wifiServiceClient) Update(ctx context.Context, in *UpdateWifi, opts ...grpc.CallOption) (*Wifi, error) {
	out := new(Wifi)
	err := c.cc.Invoke(ctx, WifiService_Update_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wifiServiceClient) UpdatePatch(ctx context.Context, in *UpdatePatchWifi, opts ...grpc.CallOption) (*Wifi, error) {
	out := new(Wifi)
	err := c.cc.Invoke(ctx, WifiService_UpdatePatch_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wifiServiceClient) Delete(ctx context.Context, in *WifiPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, WifiService_Delete_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// WifiServiceServer is the server API for WifiService service.
// All implementations must embed UnimplementedWifiServiceServer
// for forward compatibility
type WifiServiceServer interface {
	Create(context.Context, *CreateWifi) (*Wifi, error)
	GetById(context.Context, *WifiPrimaryKey) (*Wifi, error)
	GetList(context.Context, *GetListWifiRequest) (*GetListWifiResponse, error)
	Update(context.Context, *UpdateWifi) (*Wifi, error)
	UpdatePatch(context.Context, *UpdatePatchWifi) (*Wifi, error)
	Delete(context.Context, *WifiPrimaryKey) (*empty.Empty, error)
	mustEmbedUnimplementedWifiServiceServer()
}

// UnimplementedWifiServiceServer must be embedded to have forward compatible implementations.
type UnimplementedWifiServiceServer struct {
}

func (UnimplementedWifiServiceServer) Create(context.Context, *CreateWifi) (*Wifi, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedWifiServiceServer) GetById(context.Context, *WifiPrimaryKey) (*Wifi, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedWifiServiceServer) GetList(context.Context, *GetListWifiRequest) (*GetListWifiResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedWifiServiceServer) Update(context.Context, *UpdateWifi) (*Wifi, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedWifiServiceServer) UpdatePatch(context.Context, *UpdatePatchWifi) (*Wifi, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePatch not implemented")
}
func (UnimplementedWifiServiceServer) Delete(context.Context, *WifiPrimaryKey) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedWifiServiceServer) mustEmbedUnimplementedWifiServiceServer() {}

// UnsafeWifiServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to WifiServiceServer will
// result in compilation errors.
type UnsafeWifiServiceServer interface {
	mustEmbedUnimplementedWifiServiceServer()
}

func RegisterWifiServiceServer(s grpc.ServiceRegistrar, srv WifiServiceServer) {
	s.RegisterService(&WifiService_ServiceDesc, srv)
}

func _WifiService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateWifi)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WifiServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WifiService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WifiServiceServer).Create(ctx, req.(*CreateWifi))
	}
	return interceptor(ctx, in, info, handler)
}

func _WifiService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WifiPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WifiServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WifiService_GetById_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WifiServiceServer).GetById(ctx, req.(*WifiPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _WifiService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListWifiRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WifiServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WifiService_GetList_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WifiServiceServer).GetList(ctx, req.(*GetListWifiRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WifiService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateWifi)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WifiServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WifiService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WifiServiceServer).Update(ctx, req.(*UpdateWifi))
	}
	return interceptor(ctx, in, info, handler)
}

func _WifiService_UpdatePatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePatchWifi)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WifiServiceServer).UpdatePatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WifiService_UpdatePatch_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WifiServiceServer).UpdatePatch(ctx, req.(*UpdatePatchWifi))
	}
	return interceptor(ctx, in, info, handler)
}

func _WifiService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WifiPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WifiServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WifiService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WifiServiceServer).Delete(ctx, req.(*WifiPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// WifiService_ServiceDesc is the grpc.ServiceDesc for WifiService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var WifiService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "qr_type_service.WifiService",
	HandlerType: (*WifiServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _WifiService_Create_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _WifiService_GetById_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _WifiService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _WifiService_Update_Handler,
		},
		{
			MethodName: "UpdatePatch",
			Handler:    _WifiService_UpdatePatch_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _WifiService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "wifi_service.proto",
}
