
CREATE TABLE IF NOT EXISTS "users" (
    "id" uuid PRIMARY KEY,
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "phone_number" VARCHAR UNIQUE NOT NULL,
    "email" VARCHAR,
    "country" VARCHAR,
    "website" VARCHAR,
    "created_at" TIMESTAMP DEFAULT (now()),
    "updated_at" TIMESTAMP DEFAULT (now())
);
